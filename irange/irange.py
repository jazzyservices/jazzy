"""Inclusive range."""

def irange(*args):
    """Return an inclusive range.

    >>> list(irange(3))
    [0, 1, 2, 3]
    >>> list(irange(8,8))
    [8]
    >>> list(irange(1,-1,-1))
    [1, 0, -1]
    """
    if len(args) == 1:
        return range(args[0] + 1)
    if len(args) == 2:
        return range(args[0], args[1] + 1)
    if len(args) == 3:
        return range(args[0], args[1] + args[2], args[2])
    return range(*args)

inclusive = irange
