# coding=ascii
"""Jazzy Cat.

Concatenate names and expressions in a compact way.
(c) Jazzy Services Ltd 2018
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
import codecs
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with codecs.open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='jazzycat',  # Required
    version='0.0.1',  # Required
    description='Concatenate names and expressions easily',  # Required
    long_description=long_description,  # Optional
    long_description_content_type='text/markdown',  # Optional (see note above)
    url='https://github.com/jazzy/jazzycat',  # Optional
    author='John Cooke',  # Optional
    author_email='john.cooke@phonecoop.coop',  # Optional
    keywords='utilities string concat',  # Optional
    license='MIT License',
    py_modules       = ["jazzycat"],
)
