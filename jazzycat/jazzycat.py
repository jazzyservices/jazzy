# coding=ascii
"""Class to concatenate simple objects together.

Concatenate some words into a string separated by dots:
>>> Cat().sax.parser.open
'sax.parser.open'

To use a Cat where one might expect a string,
use the unary '+' operator to turn a Cat into a string.
>>> len(+Cat().hello.world)
11

"""
# pylint: disable=too-few-public-methods, no-self-argument, invalid-name


class Cat(object):
    """Easily join words together by using dot notation."""
    def __init__(my, sep='.'):
        my._sep = sep
        my._value = ''

    @classmethod
    def _withvalue(cls, sep, value):
        """Set the value of the concatenation."""
        new = cls(sep)
        new._value = value  # pylint: disable=protected-access
        return new

    def __bool__(my):
        return bool(my._value)

    def _cmp(my, other):
        if isinstance(other, Cat):
            other = other._value
        if not isinstance(other, str):
            return False
        return -1 if (my._value < other) else int(my._value > other)

    def __lt__(my, other):
        return my._cmp(other) < 0

    def __le__(my, other):
        return my._cmp(other) <= 0

    def __eq__(my, other):
        return my._cmp(other) == 0

    def __ne__(my, other):
        return my._cmp(other) != 0

    def __ge__(my, other):
        return my._cmp(other) >= 0

    def __gt__(my, other):
        return my._cmp(other) > 0

    def __hash__(my):
        return hash(my._value)

    def __getattr__(my, name):
        if my._value:
            new_value = my._sep.join((my._value, name))
        else:
            new_value = name
        return my._withvalue(my._sep, new_value)

    # Call the Cat to concatenate expressions
    #
    # >>> name, surname = 'john', 'wayne'
    # >>> Cat().call.me(name, surname) => 'call.me.john.wayne'
    # >>> Cat().meet.at(6) => 'meet.at.6'
    def __call__(my, *args):
        sep = my._sep
        if my._value:
            args = (my._value, ) + args
        new_value = sep.join(str(x) for x in args)
        return my._withvalue(sep, new_value)

    # Use Cat[expr] to change the separator.
    def __getitem__(my, key):
        if not isinstance(key, int):
            new_sep = str(key)
        # special case: negative means '-'
        # otherwise use the unichar
        elif key < 0:
            new_sep = '-'
        else:
            new_sep = chr(key)
        return my._withvalue(new_sep, my._value)

    def __pos__(my):
        """The unary '+' operator: turns the Cat into a string.

        >>> +Cat().www.bbc.co.uk == 'www.bbc.co.uk'
        """
        return my._value

    def __str__(my):
        return my._value

    def __repr__(my):
        return repr(my._value)


class Cat0(Cat):
    """A Cat that prepends its separator."""
    def __getattr__(my, name):
        new_value = my._sep + name
        return Cat._withvalue(my._sep, new_value)


ROOT = Cat0('/')
